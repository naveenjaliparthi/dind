#! /bin/bash

. image_info
. .version_info
echo "In test job args are: IMAGE_FULL_PATH: $IMAGE_FULL_PATH IMAGE_TAG: $IMAGE_TAG TEST_CONTAINER: $TEST_CONTAINER"
docker run -dit --name $TEST_CONTAINER $IMAGE_FULL_PATH:$IMAGE_TAG_SHA

if [ -d test ]; then
  echo "copying test to container"
  docker cp test "${TEST_CONTAINER}:${TEST_DESTINATION}"
fi
    
sleep $SLEEP_SECS 

if [ -z "$TEST_CMD" ]; then #notice that the var has double quotes, this is coz of colon in test cmd
  echo "no test cmd specified with use default uname -a"
  TEST_CMD="uname -a"
fi
docker exec "${TEST_CONTAINER}" ${TEST_CMD} #notice no quotes, this is CRAZY
docker stop "$TEST_CONTAINER"
docker rm "$TEST_CONTAINER"