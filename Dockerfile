FROM docker:20.10.6-git
# Add script
COPY *.sh /usr/bin/
RUN chmod a+x /usr/bin/*.sh && apk add --no-cache bash curl


CMD ["/bin/bash"]