#! /bin/bash

cwd=$(pwd)
echo $cwd
if [ "$UBI_LOGIN" == "true" ]; then docker login -u "rparekh@indigoconsulting.com" -p "$UBI_REGISTRY_PASSWORD" $UBI_REGISTRY; fi
cat .version_info
. .version_info

echo "next version is $RELEASE_VERSION"
echo "RELEASE_URL=$CI_PROJECT_URL/-/releases" > build_info
echo "RELEASE_DESC=\"$(uname -mo) binary\"" >> build_info
echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
echo "RELEASE_VERSION=$RELEASE_VERSION" >> build_info
cat build_info
. image_info
echo "In build job args are FROM_IMAGE_FULL_PATH $FROM_IMAGE_FULL_PATH IMAGE_NAME $IMAGE_NAME IMAGE_TAG $IMAGE_TAG IMAGE_TAG_SHA $IMAGE_TAG_SHA"
if [[ ! -z "${FROM_IMAGE_FULL_PATH}" ]]; then
  docker build --pull --build-arg FROM_IMAGE_FULL_PATH=$FROM_IMAGE_FULL_PATH --tag $IMAGE_FULL_PATH:$IMAGE_TAG --tag $IMAGE_FULL_PATH:$IMAGE_TAG_SHA .
else
  docker build --pull --tag $IMAGE_FULL_PATH:$IMAGE_TAG --tag $IMAGE_FULL_PATH:$IMAGE_TAG_SHA .
fi
